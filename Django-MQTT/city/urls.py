from django.urls import path
from .views import *

app_name = 'city'

urlpatterns = [
    path('index/', index, name='index'),
    path('add/', add, name='add'),
    path('update/<str:slug>', update, name='update'),
    path('delete/<str:slug>', delete, name='delete'),
]
