from django.db import models
from django.urls import reverse
from django.utils.text import slugify


class City(models.Model):
    city = models.CharField(max_length=100, unique=True)
    temperature = models.IntegerField()
    condition = models.TextField()
    slug = models.SlugField(unique=True, editable=False, max_length=130)

    def __str__(self):
        return self.city

    def get_update_url(self):
        return reverse('city:update', kwargs={'slug': self.slug})

    def get_delete_url(self):
        return reverse('city:delete', kwargs={'slug': self.slug})

    def save(self, *args, **kwargs):
        self.slug = slugify(self.city)
        return super(City, self).save(*args, **kwargs)
