from django.shortcuts import render, get_object_or_404, redirect, Http404
from .models import City
from .forms import CityForm

import time

import paho.mqtt.client as mqtt

error_404_delay = 15

broker = 'raspberrypi'
port = 1883

index_client = add_client = update_client = delete_client = None


def on_message(client, userdata, msg):
    global index_client, add_client, update_client, delete_client
    print(msg.topic + " " + msg.payload.decode("utf-8"))
    if msg.topic == 'index/client':
        index_client = msg.payload.decode("utf-8")
    elif msg.topic == 'add/client':
        add_client = msg.payload.decode("utf-8")
    elif msg.topic == 'update/client':
        update_client = msg.payload.decode("utf-8")
    elif msg.topic == 'delete/client':
        delete_client = msg.payload.decode("utf-8")


client = mqtt.Client()

client.on_message = on_message
client.connect(broker, port)
client.subscribe('index/client')
client.subscribe('add/client')
client.subscribe('update/client')
client.subscribe('delete/client')

client.loop_start()


def index(request):
    if request.user.is_authenticated:
        global index_client, client

        client.publish('index/connected', 'connected')

        t_end = time.time() + error_404_delay
        while time.time() < t_end:
            if index_client is not None:
                break

        if index_client is not None:
            cities = list(reversed(City.objects.all()))
            index_client = None

            context = {
                'cities': cities,
                'user': request.user.username,
                'user_permission': request.user.is_superuser,
            }

            return render(request, 'city/index.html', context)
        else:
            raise Http404()
    else:
        raise Http404()


def add(request):
    if request.user.is_superuser:
        global add_client, client

        client.publish('add/connected', 'connected')

        t_end = time.time() + error_404_delay
        while time.time() < t_end:
            if add_client is not None:
                break

        if add_client is not None:
            form = CityForm(request.POST or None)
            add_client = None

            context = {
                'form': form,
                'user': request.user.username
            }

            return render(request, 'city/form.html', context)
        else:
            raise Http404()
    else:
        raise Http404()


def update(request, slug):
    if request.user.is_superuser:
        global update_client, client

        client.publish('update/connected', f'connected,{slug}')

        t_end = time.time() + error_404_delay
        while time.time() < t_end:
            if update_client is not None:
                break

        if update_client is not None:
            city = get_object_or_404(City, slug=slug)
            form = CityForm(request.POST or None, instance=city)
            update_client = None

            context = {
                'city': city,
                'form': form,
                'user': request.user.username
            }

            return render(request, 'city/form.html', context)
        else:
            raise Http404()
    else:
        raise Http404()


def delete(request, slug):
    if request.user.is_superuser:
        global delete_client, client

        client.publish('delete/connected', f'connected,{slug}')

        t_end = time.time() + error_404_delay
        while time.time() < t_end:
            if delete_client is not None:
                break

        if delete_client is not None:
            delete_client = None
            return redirect('city:index')
        else:
            raise Http404()
    else:
        raise Http404()
