import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "weather.settings")
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()
os.environ["DJANGO_ALLOW_ASYNC_UNSAFE"] = "true"

import paho.mqtt.client as mqtt
from django.shortcuts import get_object_or_404

from city.models import City

broker = 'raspberrypi'
port = 1883
sub_topic = '#'
active_users = [] # her kullanıcı girişinde buraya ekle, her çıkışında çıkar

update_post_slug = ''


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        print('Connected successfully! \n')
    else:
        print('Error: ' + str(rc))


def on_message(client, userdata, msg):
    global update_post_slug

    main_message = msg.payload.decode("utf-8")
    print(msg.topic + " -> " + main_message)

    # ----- INDEX -----
    if msg.topic == 'index/connected':
        send_data('index/client', 'connected')
    # ----- INDEX -----

    # ----- ADD -----
    if msg.topic == 'add/connected':
        send_data('add/client', 'connected,add')

    elif msg.topic == 'add/new-city':
        post_variables = main_message.split(',')
        city = City.objects.create(city=post_variables[0], temperature=post_variables[1], condition=post_variables[2])
        send_data('add/client', 'done')
        send_data('index/client', f'add,{post_variables[0]},{post_variables[1]},{post_variables[2]},{city.get_update_url()},{city.get_delete_url()}')
    # ----- ADD -----

    # ----- UPDATE -----
    if msg.topic == 'update/connected':
        update_post_slug = main_message.split(',')[1]
        send_data('update/client', 'connected,update')

    elif msg.topic == 'update/done':
        updated_city = get_object_or_404(City, slug=update_post_slug)
        old_city_name = updated_city.city

        post_variables = main_message.split(',')
        updated_city.city = post_variables[0]
        updated_city.temperature = post_variables[1]
        updated_city.condition = post_variables[2]

        updated_city.save()
        update_post_slug = ''

        send_data('update/client', 'done')
        send_data('index/client', f'update,{post_variables[0]},{post_variables[1]},{post_variables[2]},{updated_city.get_update_url()},{old_city_name},{updated_city.get_delete_url()}')
    # ----- UPDATE -----

    # ----- DELETE -----
    if msg.topic == 'delete/connected':
        deleted_city = get_object_or_404(City, slug=main_message.split(',')[1])
        city_name = deleted_city.city
        deleted_city.delete()
        send_data('delete/client', 'delete')
        send_data('index/client', f'delete,{city_name}')
    # ----- DELETE -----

    # ----- LOGIN -----
    if msg.topic == 'login/connected':
        send_data('login/client', 'connected,login')
    # ----- LOGIN -----

    # ----- REGISTER -----
    if msg.topic == 'register/connected':
        send_data('register/client', 'connected,register')
    # ----- REGISTER -----

    # ----- LOG OUT -----
    if msg.topic == 'logout/connected':
        send_data('logout/client', 'logout')
    # ----- LOG OUT -----


client = mqtt.Client()

client.on_connect = on_connect
client.on_message = on_message

client.connect(broker, port)
client.subscribe(sub_topic)


def send_data(topic, msg):
    global client
    client.publish(topic, msg)


client.loop_forever()
