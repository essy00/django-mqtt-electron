from django.shortcuts import render, redirect, Http404
from .forms import LoginForm, RegisterForm
from .models import Profile
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.core.serializers.json import DjangoJSONEncoder
from django.contrib.auth.signals import user_logged_in, user_logged_out
from django.dispatch import receiver
from django.contrib.auth import get_user_model

import json
import time

import paho.mqtt.client as mqtt

User = get_user_model()

error_404_delay = 15

broker = 'raspberrypi'
port = 1883

register_client = login_client = logout_client = None


def on_message(client, userdata, msg):
    global register_client, login_client, logout_client
    if msg.topic == 'register/client':
        register_client = msg.payload.decode("utf-8")
    elif msg.topic == 'login/client':
        login_client = msg.payload.decode("utf-8")
    elif msg.topic == 'logout/client':
        logout_client = msg.payload.decode("utf-8")


client = mqtt.Client()

client.on_message = on_message
client.connect(broker, port)
client.subscribe('login/client')
client.subscribe('register/client')
client.subscribe('logout/client')

client.loop_start()


def get_active_users(active_users_string):
    for user in User.objects.all():
        if user.profile.is_online:
            active_users_string += f'{user.username},'
    active_users_string = active_users_string[:-1]
    return active_users_string


@receiver(user_logged_in)
def got_online(sender, user, request, **kwargs):    
    user.profile.is_online = True
    user.profile.save()


@receiver(user_logged_out)
def got_offline(sender, user, request, **kwargs):   
    user.profile.is_online = False
    user.profile.save()


def login_view(request):
    global login_client, client

    active_users = get_active_users('')

    client.publish('login/connected', f'connected')

    t_end = time.time() + error_404_delay
    while time.time() < t_end:
        if login_client is not None:
            break

    if login_client is not None:
        form = LoginForm(request.POST or None)

        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            Profile.objects.get_or_create(user=user)
            login(request, user)
            return redirect('city:index')

        context = {
            'form': form,
            'title': 'Log In',
            'active_users': active_users,
        }

        return render(request, "accounts/form.html", context)
    else:
        raise Http404()


def register_view(request):
    global register_client, client

    active_users = get_active_users('')

    client.publish('register/connected', f'connected')

    t_end = time.time() + error_404_delay
    while time.time() < t_end:
        if register_client is not None:
            break

    if register_client is not None:
        form = RegisterForm(request.POST or None)

        if form.is_valid():
            user = form.save(commit=False)
            password = form.cleaned_data.get('confirm_password')
            user.set_password(password)
            user.save()
            login_user = authenticate(username=user.username, password=password)
            Profile.objects.get_or_create(user=login_user)
            login(request, login_user)
            return redirect('city:index')

        context = {
            'form': form,
            'title': 'Register',
            'active_users': active_users,
        }

        return render(request, 'accounts/form.html', context)
    else:
        raise Http404()


def logout_view(request):
    global logout_client, client

    client.publish('logout/connected', f'connected')

    t_end = time.time() + error_404_delay
    while time.time() < t_end:
        if logout_client is not None:
            break

    if logout_client is not None:
        username = request.user.username
        logout(request)
        return redirect('home:home')
    else:
        raise Http404()
