from django.contrib import admin
from django.contrib.auth.admin import UserAdmin

from .models import User


class AccountAdmin(UserAdmin):
    list_display = ('username', 'last_login', 'is_admin', 'is_staff')
    search_fields = ('username',)
    readonly_fields = ('id',)

    filter_horizontal = ()
    list_filter = ()
    fieldsets = ()


admin.site.register(User, AccountAdmin)
